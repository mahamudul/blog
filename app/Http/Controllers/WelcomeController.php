<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $published_blog=DB::table('tbl_blog')
            ->join('tbl_category','tbl_blog.category_id','=','tbl_category.category_id')
            ->where('tbl_blog.publication_status',1)
            ->select('tbl_blog.*','tbl_category.category_name')
            ->get();

        $home_content= view('pages.home')
            ->with('published_blog',$published_blog);
        //$category= view('pages.category');
        $recent = view('pages.recent');
        $cat = view('pages.cat');
        return view('master')
                ->with('content',$home_content)
                ->with('recent',$recent)
               // ->with('category',$category)
                ->with('cat',$cat);
    }
    public function portfolio()
    {
        $home_portfolio= view('pages.portfolio');
        return view('master')
                ->with('portfolio',$home_portfolio);
    }
    public function service()
    {
        $home_service= view('pages.service');
        return view('master')
                ->with('service',$home_service);
    }
    public function contact()
    {
        $home_contact= view('pages.contact');
        return view('master')
                ->with('contact',$home_contact);
    }

    public function blog_details($blog_id)
    {
        $blog_info_by_id=DB::table('tbl_blog')
            ->join('tbl_category','tbl_blog.category_id','=','tbl_category.category_id')
            ->where('tbl_blog.blog_id',$blog_id)
            ->select('tbl_blog.*','tbl_category.category_name')
            ->first();

        $blog_details= view('pages.blog_details')
            ->with('blog_info',$blog_info_by_id);
        $recent = view('pages.recent');
        $cat = view('pages.cat');
        return view('master')
            ->with('content',$blog_details)
            ->with('recent',$recent)
            // ->with('category',$category)
            ->with('cat',$cat);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
