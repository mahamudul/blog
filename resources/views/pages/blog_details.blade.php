@extends('master')
@section('content')
    <div class="post_section">

        <div class="post_date">
            30<span>Nov</span>
        </div>
            <div class="post_content">

                <h3>{{$blog_info->blog_title}}</h3>


                <strong>Author:</strong> Steven | <strong> Category: &nbsp {{$blog_info->category_name }}</strong></a>

                <a href="#" target="_parent"><img src="{{URL::to($blog_info->blog_image) }}" width="400px" alt="image" /></a>
                <?php echo $blog_info->blog_long_description ?>
            </div>
        <div class="cleaner"></div>
    </div>
@endsection